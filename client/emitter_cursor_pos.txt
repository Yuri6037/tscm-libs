--@name EmitterCursorPosLib
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

--Function that returns intersection t and position from a ray and a plane
function util.IntersectRayWithPlane(rayorigin, raydir, planepos, planenormal)
	local t = planenormal:Dot(planepos - rayorigin) / planenormal:Dot(raydir)
	if (t < 0) then
		return nil
	end
	local p = rayorigin + t * raydir
	return p, t
end

function util.MousePos3D2D(origin, angle, scale)
	local raystart = ents.player():eyePos()
	local raydir = ents.player():aimVector()
	local normal = -angle:Up()

	local endpos = util.IntersectRayWithPlane(raystart, raydir, origin, normal)
	if (not(endpos == nil)) then
		local pos = worldToLocal(endpos, ents.player():eyeAngles(), origin, angle)
		return pos.X / scale, -pos.Y / scale
	end
	return 0, 0
end